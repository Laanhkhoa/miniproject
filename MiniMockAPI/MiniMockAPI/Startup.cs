using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MiniMockAPI.Data;
using MiniMockAPI.Extentions;
using MiniMockAPI.Models;
using System;

namespace MiniMockAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.RegisterService();
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase(databaseName: "DatabaseNe"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

/*            app.Use(async (context, next) =>
            {
                if (await apiContext.CheckUser(context.Request.Headers[User.UserString]))
                    await next.Invoke();
                else
                {
                    context.Response.StatusCode = 301;
                    await context.Response.WriteAsync("Chua dang nhap. Chay ra dang nhap di");
                }
               
            });*/

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
