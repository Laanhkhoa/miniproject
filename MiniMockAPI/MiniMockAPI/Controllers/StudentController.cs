﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiniMockAPI.Data;
using MiniMockAPI.Interfaces;
using MiniMockAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MiniMockAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly ApiContext _context;

        private readonly IUnitOfWork _unitOfWork;

        public StudentController(ApiContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        // GET: api/<StudentController>
        [HttpGet]
        public async Task<List<Student>> Get()
        {
            return await _context.Students.ToListAsync();
        }

        // GET api/<StudentController>/5
        [HttpGet("{id}")]
        public async Task<Student> Get(int id)
        {
            return await _context.Students.FindAsync(id);
        }

        // POST api/<StudentController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Student student)
        {
            try
            {
                await _context.Students.AddAsync(student);
                await _context.SaveChangesAsync();
                return Ok(student);
            }
            catch
            {
                return NoContent();
            }
        }

        // PUT api/<StudentController>/5
        [HttpPut("{id}")]
        public async Task<bool> Put(int id, [FromBody] Student student)
        {
            student.Id = id;

            return await _unitOfWork.UpdateStudent(student);
        }

        // DELETE api/<StudentController>/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            return await _unitOfWork.RemoveStudent(id);
        }
    }
}
