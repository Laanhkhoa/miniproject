﻿using Microsoft.Extensions.DependencyInjection;
using MiniMockAPI.Interfaces;

namespace MiniMockAPI.Extentions
{
    public static class ServiceExtention
    {
        public static IServiceCollection RegisterService(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
