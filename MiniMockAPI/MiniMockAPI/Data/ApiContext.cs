﻿using Microsoft.EntityFrameworkCore;
using MiniMockAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniMockAPI.Data
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<User> Users { get; set; }

        public async Task<bool> CheckUser(string username)
        {
            var check = await Users.FirstOrDefaultAsync(item => item.UserName.Equals(username));

            if (check == null)
                return false;

            return true;
        }
    }
}
