﻿using MiniMockAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniMockAPI.Data
{
    public class StudentSeedDatacs
    {
        public static void SeedData(ApiContext context)
        {
            var studentList = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Name = "Luke",
                    Age = 18
                },
                new Student
                {
                    Id = 2,
                    Name = "Lux",
                    Age = 19
                },
                new Student
                {
                    Id = 3,
                    Name = "Kid",
                    Age = 18
                },
                new Student
                {
                    Id = 4,
                    Name = "Haru",
                    Age = 20
                },
            };

            var userList = new List<User>
            {
                new User
                {
                    UserName = "MinhDinh",
                    Password = "123",
                },
                new User
                {
                    UserName = "MinhDinh1",
                    Password = "123",
                },
                new User
                {
                    UserName = "MinhDinh2",
                    Password = "123",
                },
                new User
                {
                    UserName = "MinhDinh3",
                    Password = "123",
                },
            };

            context.Users.AddRange(userList);
            context .Students.AddRange(studentList);

            context.SaveChanges();
        }
    }
}
