﻿using MiniMockAPI.Models;
using System;
using System.Threading.Tasks;

namespace MiniMockAPI.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> Authentication(string username);

        Task<bool> UpdateStudent(Student student);

        Task<bool> RemoveStudent(int id);
    }
}
