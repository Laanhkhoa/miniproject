﻿using Microsoft.EntityFrameworkCore;
using MiniMockAPI.Data;
using MiniMockAPI.Interfaces;
using MiniMockAPI.Models;
using System;
using System.Threading.Tasks;

namespace MiniMockAPI
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApiContext _apiContext;

        public UnitOfWork(ApiContext apiContext)
        {
            _apiContext = apiContext;
        }

        public async Task<bool> Authentication(string username)
        {
            var check = await _apiContext.Users.FirstOrDefaultAsync(item => item.UserName.Trim().Equals(username.Trim()));

            if (check == null)
                return false;

            return true;
        }

        public void Dispose()
        {
            _apiContext.Dispose();
        }

        public async Task<bool> RemoveStudent(int id)
        {
            try
            {
                var student = await _apiContext.Students.FirstOrDefaultAsync(item => item.Id == id);
                _apiContext.Students.Remove(student);
                await _apiContext.SaveChangesAsync();

                return true;
            }
            catch(ArgumentNullException)
            {
                _apiContext.Dispose();

                return false;
            }
        }

        public async Task<bool> UpdateStudent(Student student)
        {
            try
            {
                _apiContext.Students.Update(student);
                await _apiContext.SaveChangesAsync();

                return true;
            }
            catch(DbUpdateConcurrencyException)
            {
                _apiContext.Dispose();

                return false;
            }  
        }
    }
}
